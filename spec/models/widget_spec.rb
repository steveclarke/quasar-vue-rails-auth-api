require 'rails_helper'

RSpec.describe Widget, type: :model do
  it 'has a valid factory' do
    expect(build(:widget)).to be_valid
  end

  it { should validate_presence_of(:title) }
end
