FactoryBot.define do
  factory :user do
    email      { Faker::Internet.email }
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    role       { [0,1,2].sample }
    active     { true }
    password   { 'Pa$$word1' }
  end
end
