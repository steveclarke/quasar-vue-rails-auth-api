FactoryBot.define do
  factory :widget do
    title { Faker::Commerce.product_name }
  end
end
