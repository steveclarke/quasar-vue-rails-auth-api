class User < ApplicationRecord
  has_secure_password

  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false }
  
  validates :password,
            length: { minimum: 8 }
  
  enum role: { user: 0, admin: 1, superuser: 2 }

  def jwt_payload
    {
      user: {
        email: email,
        id: id,
        firstName: first_name,
        lastName: last_name,
        role: role
      }
    }
  end
end
