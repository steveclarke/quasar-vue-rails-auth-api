class SignInController < ApplicationController
  before_action :authorize_access_request!, only: [ :destroy ]
  rescue_from ActiveRecord::RecordNotFound, with: :render_record_not_found

  def create
    user = User.find_by!(email: params[:email])
  
    if user.authenticate(params[:password])
      payload = user.jwt_payload

      session = JWTSessions::Session.new(
        payload: payload,
        refresh_by_access_allowed: true,
        namespace: "user_#{user.id}"
      )
      
      render json: session.login
    else
      render_not_authorized
    end
  end

  def destroy
  end
end
