class WidgetsController < ApplicationController
  before_action :authorize_access_request!

  def index
    @widgets = Widget.all
    render json: @widgets
  end
end
