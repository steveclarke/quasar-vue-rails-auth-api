class ApplicationController < ActionController::API
  include JWTSessions::RailsAuthorization
  rescue_from JWTSessions::Errors::Unauthorized, with: :render_not_authorized

  private

  def render_not_authorized
    render(
      json: {
        error: {
          message: 'Unauthorized',
          code: 'unauthorized'
        }
      },
      status: :unauthorized
    )
  end

  def render_record_not_found(exception)
    render(
      json: {
        error: {
          message: exception.message,
          code: 'record_not_found'
        }
      },
      status: :not_found
    )
  end
end
