Rails.application.routes.draw do
  post   'auth', to: 'sign_in#create'
  delete 'auth', to: 'sign_in#destroy'

  resources :widgets
end
