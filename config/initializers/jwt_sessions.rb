JWTSessions.encryption_key = ENV.fetch('JWT_SECRET_KEY')
JWTSessions.access_exp_time = ENV.fetch('JWT_EXPIRES_IN')
JWTSessions.refresh_exp_time = ENV.fetch('JWT_REFRESH_EXPIRES_IN')