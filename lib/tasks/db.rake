namespace :db do
  desc 'Load a set of sample data, useful for development'
  task sample_data: :environment do
    sample_data = Rails.root.join('db', 'sample_data.rb')
    load(sample_data) if sample_data
  end
end