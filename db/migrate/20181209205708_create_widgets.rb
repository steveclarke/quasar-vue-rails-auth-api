class CreateWidgets < ActiveRecord::Migration[5.2]
  def change
    create_table :widgets, id: :uuid do |t|
      t.string :title

      t.timestamps
    end
  end
end
