class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: :uuid do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.integer :role
      t.boolean :active
      t.string :password_digest

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
