if Rails.env.production?
  puts 'This task is not meant to run in production!'
  exit!
end

puts 'Deleting existing data...'
Widget.delete_all
User.delete_all

puts 'Users...'
# Create regular users
FactoryBot.create_list(:user, 10, role: :user)
# Create admin users
FactoryBot.create_list(:user, 2, role: :admin)
# Create super user
FactoryBot.create(
  :user,
  role: :superuser,
  email: 'foo@bar.com',
  password: 'Pa$$word1'
)

puts 'Widgets...'
FactoryBot.create_list(:widget, 10)

puts 'Done.'

